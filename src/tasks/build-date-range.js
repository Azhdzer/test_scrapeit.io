/**
 * The function 'buildDateRange' takes a single parameter 'gapInDays',
 * showing the difference in days and should create two dates.
 * The first date is the current date of the function invoking.
 * The second date is the one that is in the past by the difference in days.
 * The function returns an object with two properties 'today' and 'previous'.
 * See the expected result below.
 *
 * Example of result:
 * {
 *   today: '2023-03-04'    // it's the current date
 *   previous: '2023-03-01' // date in the past
 * }
 *
 * IMPORTANT NOTICE!
 * Use only the methods of the 'momentjs' to biuld and manipulate the dates.
 * https://momentjs.com/docs/
 */

const moment = require('moment');

const gapInDays = 3;

const buildDateRange = (gap) => {
  const previous = moment().subtract(gap, 'days').format('YYYY-MM-DD');
  // .subtract(Number, String)  вычитая время

  return {
    today: moment().format('YYYY-MM-DD'),
    // .format("dddd, MMMM Do YYYY, h:mm:ss a");  выборка формата

    previous,
  };
};

// проверка
// console.log(buildDateRange(gapInDays));

// экспорт функции для других файлов
module.exports = buildDateRange(gapInDays);
