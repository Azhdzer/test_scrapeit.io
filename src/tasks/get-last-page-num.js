/**
 * The function 'getLastPageNum' takes a single parameter 'paginationText' -
 * the array of text values scraped from pagination block on amazon.de.
 *
 * The function should return the number of the last page in integer format.
 * Expected result: 42
 *
 * You can, but are not required to use 'lodash'
 */

const _ = require('lodash');

const paginationText = ['Zurück', '1', '2', '3', '...', '42', 'Weiter'];

const getLastPageNum = (texts) => {
  const lastPageIndex = _.findLastIndex(texts, (text) => !Number.isNaN(parseInt(text, 10)));
  // _.findLastIndex поиска последнего индекса элемента массива(!но только число,иначе NaN),
  //! isNaN(pasreInt) => parseInt()- преобразует наш элемент массива в число,
  // после isNaN проверяет число ли(isNumber не работал потому что до этого использовался parserInt)
  // и оператором не обращаем ответ, чтобы дойти до индекса с числом
  return parseInt(texts[lastPageIndex], 10);
  // Возвращаем числовой формат
};

// console.log(getLastPageNum(paginationText));

module.exports = getLastPageNum(paginationText);
