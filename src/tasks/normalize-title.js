/**
 * The function 'normalizeTitle' takes a single parameter 'title',
 * the string value, taken from booking.com and modified with several malicious symbols,
 * such as '\n', '\t', '\' and double spaces.
 *
 * The function should remove all malicious symbols,
 * and return the normalized string as shown below.
 *
 * Expected result: 'Treasure Island - TI Las Vegas Hotel & Casino, a Radisson Hotel'
 *
 * You can, but are not required to us~nr e 'lodash'
 */

const _ = require('lodash');

/* const red = '\x1b[31m';
const green = '\x1b[32m'; */

const title = '  \t Treasure  Island\t\n - \n  TI Las Vegas Hotel \\ &  Casino,\n a\t Radisson \\  Hotel\t\n';
// console.log(red, title);

const normalizeTitle = (str) => {
  let normalizedStr = str.replace(/[\n\t]/g, '');
  // replace() метод замемняет [..] на пустоту ""

  normalizedStr = normalizedStr.replace(/\\/g, ' ');
  // далее этим же методом заменяем \\

  normalizedStr = normalizedStr.replace(/\s{2,}/g, ' ');
  // заменяем двойные пробелы

  normalizedStr = _.trim(normalizedStr);
  // _.trim() удаляет начальные и конечные пробелы заданной строки

  return normalizedStr;
};

// console.log(green, normalizeTitle(title));

module.exports = normalizeTitle(title);
