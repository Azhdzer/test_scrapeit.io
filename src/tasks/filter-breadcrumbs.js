/**
 * The function 'filterBreadcrumbs' takes a single parameter 'breadcrumbs',
 * including an array of arrays, scraped from misumi.com.
 * Each nested array includes several string values from website breadcrumbs chains.
 * Some of them are duplicates.
 * The function should return an array with filtered nested arrays,
 * which includes only a unique set of breadcrumbs.
 * Sort the nested arrays, as it shown below.
 *
 * Expected result:
 * [
 *   ['Adhesives', 'Putties'],
 *   ['Adhesives', 'Putties', 'Adhesive Putty'],
 *   ['Adhesives', 'Putties', 'Epoxy Putty'],
 *   ['Adhesives', 'Putties', 'Putty']
 * ]
 *
 * You can, but are not required to use 'lodash' methods
 */

const _ = require('lodash');

const breadcrumbs = [
  ['Adhesives', 'Putties', 'Adhesive Putty'],
  ['Adhesives', 'Putties', 'Putty'],
  ['Adhesives', 'Putties', 'Adhesive Putty'],
  ['Adhesives', 'Putties', 'Epoxy Putty'],
  ['Adhesives', 'Putties', 'Epoxy Putty'],
  ['Adhesives', 'Putties', 'Putty'],
  ['Adhesives', 'Putties'],
];

const filterBreadcrumbs = (breadCrumbs) => {
  const filteredBreadcrumbs = _.chain(breadCrumbs)
    // соеденяем все массивы цепочкой, чтобы дальнейшие функции действовали на все массивы
    .uniqBy(_.join)
    // _.uniqBy() метод удаленния дубликатов. _.join конвертит наши массивы в формат строки
    .sortBy()
    // .sortBy() метод сортировки данных в алфавитном порядке
    .value();
    // .value() завершить процедуру

  return filteredBreadcrumbs;
};

// const result = filterBreadcrumbs(breadcrumbs);

// проверка
// console.log(result);

module.exports = filterBreadcrumbs(breadcrumbs);
