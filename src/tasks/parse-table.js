/**
 * The 'parseTable' function takes a single parameter '$',
 * which is a special Cheerio object.
 * This object contains a tree of nodes from the html layout,
 * which you can find along the path './__fixtures__/table.html'.
 *
 * You can find the source of that html layout here:
 * https://www.gojep.gov.jm/epps/cft/prepareViewCfTWS.do?resourceId=3053856
 *
 * The function should parse the table and get all text values from it.
 * The expeted result is an array of object, included two properties:
 * 'key' (text from 'dt' tags), and 'value' (text from 'dd' tags), see it below.
 * You shall normalize the string values by removing unuseful symbols,
 * such as ':', '\n', ect.
 * Pay attention of the property 'PPC-NCC Categories', because it should contain
 * an array of two values.
 *
 * Expected result:
 * [
 *   { key: 'Bid submission deadline in (days/hours)', value: '27/23' },
 *   { key: 'Name of procuring entity', value: 'Southern Regional Health Authority' },
 *   { key: 'Title', value: 'Private Security Services for Health Facilities' },
 *   { key: 'Competition unique ID', value: '1203/275' },
 *   { key: 'Evaluation Mechanism', value: 'LCS' },
 *   { key: 'Description', value: 'Private Security Services for Health Facilities' },
 *   { key: 'Procurement Type', value: 'Services' },
 *   { key: 'Services sub-type', value: 'Non-Consulting' },
 *   { key: 'Procurement Method', value: 'Open - NCB' },
 *   { key: 'Re-Tender flag', value: 'Re-Tender' },
 *   {
 *     key: 'PPC-NCC Categories',
 *     value: [
        'S0530-Safety and Security Services - Guard Services',
        'S0550-Safety and Security Services - other'
      ],
    },
 * ]
 *
 * Most likely, these methods could be useful:
 * https://cheerio.js.org/docs/api/classes/Cheerio#map
 * https://cheerio.js.org/docs/api/classes/Cheerio#text
 * https://cheerio.js.org/docs/api/classes/Cheerio#contents
 */
// const _ = require('lodash');
const fs = require('fs');
const cheerio = require('cheerio');

const nodeTree = cheerio.load(fs.readFileSync('C:/Users/aaazh/Desktop/test_scr/js-applicants-tests/__fixtures__/table.html', 'utf-8'));
// загрузка html

const parseTable = ($) => {
  const tableData = [];
  // массив для сохранения парсинга

  $('dt').each((index, element) => {
    // $('dt') - итерация по элементам ключу dt,  .each(масив,итерация)
    // ?? это библиотека lodash  но при этом выдет, что библ не испл.

    const key = $(element).text().replace(/[:\n]/g, '').trim();
    // передаём key текст из "dt", .replace заменяет, трим удаляет пробелы

    const valueElement = $(element).next();
    // передаём следующие значение <dt>, .next - двигает к некст элемент на том же уровне иерархии

    let value;
    // для <dd>

    if (key === 'PPC-NCC Categories') {
      // зная заранее, ищем проблемнный элемент

      value = valueElement.contents().map((_index, _element) => {
        // .contents()- вовзращает все теги,.map() - делаем итеррацию по каждому тегу
        const text = $(_element).text().trim();
        return text || undefined;
      // возврощаю текст, если нету то undefined
      }).get().filter(Boolean);
      // .get() преобразование в массив, а .filter - фильтрует от undefined
    } else {
      // Для остальных <dd>
      value = valueElement.text().replace(/\n/g, '').trim();
    }

    tableData.push({ key, value });
    // добавления ключа и значения в массив
  });

  return tableData;
};

// const result = parseTable(nodeTree);
// console.log(result);

module.exports = parseTable(nodeTree);
